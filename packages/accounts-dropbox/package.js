Package.describe({
  summary: "Login service for Dropbox accounts"
});

Package.on_use(function(api) {
  api.use('accounts-base', ['client', 'server']);
  // Export Accounts (etc) to packages using this one.
  api.imply('accounts-base', ['client', 'server']);
  api.use('accounts-oauth', ['client', 'server']);
  api.use('dropbox-oauth', ['client', 'server']);

  api.add_files("accts-dropbox.js");
});
