Package.describe({
  summary: "Dropbox OAuth flow"
});

Package.on_use(function(api) {
  api.use('oauth2', ['client', 'server']);
  api.use('oauth', ['client', 'server']);
  api.use('http', ['server']);
  api.use('underscore', 'client');
  api.use('templating', 'client');
  api.use('random', 'client');
  api.use('service-configuration', ['client', 'server']);
  api.use('dropbox',['client','server']);
  api.use('isolate-value', 'client');
  
  api.add_files('dropbox_configure.html', 'client');
  api.add_files('dropbox_configure.js', 'client');
  api.add_files('dropbox_server.js', 'server');
  api.add_files('dropbox_client.js', 'client');
});
