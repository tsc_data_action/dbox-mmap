// Dropbox = {};

Dropbox.requestCredential = function (options, credentialRequestCompleteCallback) {
  // support both (options, callback) and (callback).
  if (!credentialRequestCompleteCallback && typeof options === 'function') {
    credentialRequestCompleteCallback = options;
    options = {};
  }

  var config = ServiceConfiguration.configurations.findOne({service: 'dropbox'});
  if (!config) {
    credentialRequestCompleteCallback && credentialRequestCompleteCallback(new ServiceConfiguration.ConfigError("Service not configured"));
    return;
  }
  var credentialToken = Random.id();

  var scope = (options && options.requestPermissions) || [];
  var flatScope = _.map(scope, encodeURIComponent).join('+');
    
  var loginUrl =
        'https://www.dropbox.com/1/oauth2/authorize' +
        '?client_id=' + config.clientId +
        '&response_type=code' +
        '&redirect_uri=' + Meteor.absoluteUrl('_oauth/dropbox?close', {"secure": true}) +
        '&state=' + credentialToken;

  Oauth.initiateLogin(credentialToken, loginUrl, credentialRequestCompleteCallback, {width: 900, height: 450});
};

Dropbox.getClient = function() {
  var mid, accessToken, uid;
  mid = isolateValue(function () { 
    u = Meteor.user();
    if(u) return u._id; 
    return undefined;
  });
  if(mid) {
    accessToken = isolateValue(function () { return Meteor.user().services.dropbox.accessToken; });
    uid = isolateValue(function () { return Meteor.user().services.dropbox.id; });
    return new Dropbox.Client({"token":accessToken, "uid": uid});
  } else {
    return undefined;
  }  
}