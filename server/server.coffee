
try
  authinfo = JSON.parse(Assets.getText("authinfo.json"))
catch e
  console.log("failed reading private/authinfo.json, using default values")
  authinfo = {};

# the valid values are private and not checked in
# user will have to create a file in private/authinfo.json
# that looks something like:
# {
#   "dropbox": {
#     "clientId": "dropbox-clientid",
#     "secret":   "dropbox-secret"
#   }
# }

if (!authinfo.dropbox)
  authinfo.dropbox = 
    clientId: "junk"
    secret: "junk"

Accounts.loginServiceConfiguration.remove(service: "dropbox")
Accounts.loginServiceConfiguration.insert
  service: "dropbox"
  clientId: authinfo.dropbox.clientId
  secret: authinfo.dropbox.secret

