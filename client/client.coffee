
Session.set("dbdir", {})
Session.set("dbdone", false)
Session.set("lastUID", "0")
Session.set("selectedNode", 1)

tree  = null
nid   = 0
directoryWalk = (dir, dbc, f_pred, f_visit, cb) ->
  nid   = 0
  cnt = 0
  _dirWalk = (dir) ->
    cnt = cnt + 1
    dbc.readdir(dir, null, 
      (e, entries, stat, entry_stats) ->
        # visit the current node
        if f_pred(stat) 
          f_visit(stat)
        for s in entry_stats
          if s.isFolder
            _dirWalk(s.path)
          else if s.isFile
            if f_pred(s) 
              do (s) ->
                dbc.makeUrl(s.path, null, 
                  (e, r) -> 
                    s.url = r
                    f_visit(s))
        cnt = cnt - 1
        if(cnt == 0) 
          cb()
    )
  _dirWalk(dir)

buildDir = (s) ->
  dbdir = isolateValue( -> Session.get("dbdir"))
  if(s.isFolder)
    path = s.path.split("/")
    if(path.length < 2)
      id = nid++
      $('#tree1').tree(
        'appendNode',
        {
          id: id
          label: "root"
          meta: s
        })
      dbdir[path[0]] = 
        _nid_   : id
        _dir_   : s
        _files_ : {}
    else
      d = _.reduce(path[...path.length-1], ((x,y) -> x[y]), dbdir)
      pid = d._nid_
      id = nid++
      $('#tree1').tree(
        'appendNode',
        {
          id: id
          label: s.name
          meta: s
        },
        $('#tree1').tree('getNodeById',pid)
      )

      d[path[path.length-1]] = 
        _nid_   : id
        _dir_   : s
        _files_ : {}      
  if(s.isFile)
    path = s.path.split("/")
    name = path[path.length-1]
    d = _.reduce(path[...path.length-1], ((x,y) -> x[y]), dbdir)
    d._files_[name] = s
    pid = d._nid_
    id = nid++
    $('#tree1').tree(
      'appendNode',
      {
        id: id
        label: s.name
        meta: s
      },
      $('#tree1').tree('getNodeById',pid)
    )
    
  Session.set("dbdir", dbdir)

linkMindmap = (node) ->
  mmapNodeID = mapModel.getSelectedNodeId()
  newID = mapModel.getIdea().addSubIdea(mmapNodeID, "")
  mapModel.getIdea().updateTitle(newID, node.meta.name + " " + node.meta.url.url)

  
  
Meteor.startup ->
  $('#tree1').tree({
    data: [],
    onCreateLi: (node, $li) ->
      if node.meta.isFile
        $li.find('.jqtree-title').after('<i class="link-icon fa fa-link"></i>')
        $li.find('.link-icon').on("click", () -> linkMindmap(node))
        
    autoOpen: false,
    dragAndDrop: false
  })
  $('#tree1').bind('tree.dblclick', (e) -> 
    if(e.node.meta.isFile)
      window.open(e.node.meta.url.url)
    console.log(e.node))
  
  Deps.autorun ->
    if(isolateValue( -> Meteor.user()?._id)?)
      dbc = Dropbox.getClient()
      directoryWalk("", dbc, ((s) -> true) , buildDir, -> Session.set("dbdone", true))
    else
      rootNode = $('#tree1').tree('getTree').children[0]
      if rootNode
        $('#tree1').tree('removeNode', rootNode)
      Session.set("dbdir", {})
      Session.set("dbdone", false)      
  
Meteor.startup ->
  container = jQuery('#container')

  isTouch = true
  imageInsertController = new MAPJS.ImageInsertController("")
  mapModel = new MAPJS.MapModel(MAPJS.KineticMediator.layoutCalculator)
  mapModel.addEventListener('nodeSelectionChanged', 
    (nid, isSelected) -> 
      if(isSelected)
        Session.set("selectedNode", nid))

  container.mapWidget(console,mapModel, isTouch, imageInsertController)
  
  $('#linkEditWidget').linkEditWidget(mapModel)
  $('.arrow').click( -> $(this).toggleClass('active'))
  imageInsertController.addEventListener('imageInsertError', (reason) -> console.log('image insert error', reason))
  
  Deps.autorun ->
    if(isolateValue( -> Meteor.user()?._id)?)
      if(not Meteor.user()?.profile.mindmap?)
        Meteor.users.update( {_id:Meteor.userId()},{$set:{'profile.mindmap': EJSON.stringify(title:"MindMap")}})
      mmap = isolateValue( -> EJSON.parse(Meteor.user()?.profile.mindmap) )
      idea = MAPJS.content(mmap)
      idea.addEventListener('changed', (method, args) -> 
        console.log(method, args)
        # have to ignore this, otherwise, meteor reactivity immediate reverts it back
        if method != "addSubIdea"
          if not EJSON.equals(Meteor.user()?.profile.mindmap, mapModel.getIdea().clone())
            map = EJSON.stringify(mapModel.getIdea().clone())
            Deps.nonreactive -> Session.set("selectedNode", mapModel.getSelectedNodeId())
            Meteor.users.update( {_id:Meteor.userId()},{$set:{'profile.mindmap': map}})
      )
      selNode = Deps.nonreactive ( -> Session.get("selectedNode"))
      mapModel.setIdea(idea)  
      mapModel.selectNode(selNode)
    else
      idea = MAPJS.content(title: "Please Log In")
      mapModel.setIdea(idea)
  window.mapModel = mapModel
  
Template.main.events(
  "click button.addSubIdea"     : (e,t) -> mapModel.addSubIdea()
  "click button.editNode"       : (e,t) -> mapModel.editNode()
  "click button.removeSubIdea"  : (e,t) -> mapModel.removeSubIdea()
  "click button.toggleCollapse" : (e,t) -> mapModel.toggleCollapse()
)

  
  
  